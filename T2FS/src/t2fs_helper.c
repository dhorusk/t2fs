#include <memory.h>
#include "t2fs_helper.h"
#include "apidisk.h"
#include "bitmap2.h"

void initLibrary() {
	if (g_lib_init)
		return;

	g_lib_init = true;

	unsigned char tmp_sector[SECTOR_SIZE];
	read_sector(0, (void *)&tmp_sector);
	memcpy(&g_boot_block, tmp_sector, sizeof(BOOT_BLOCK));

	g_largest_dir_handle = 0;
	g_open_dirs = calloc(1, sizeof(DIR_DESCRIPTOR));
}

bool isFileOpenByHandle(FILE2 handle) {
	if((handle < 0) || (handle >= MAX_OPEN_FILES))
        return false;

    return g_open_files[handle].valid;
}

bool isFileOpenByFullPath(const char *full_path) {
	for (int i = 0; i < MAX_OPEN_FILES; i++) {
		FILE_DESCRIPTOR *cur_file = &g_open_files[i];

		if (!cur_file->valid)
			continue;

		if (strcmp(full_path, cur_file->file_full_path) == 0)
			return true;
	}

	return false;
}

void removeFromOpenFiles(FILE2 handle) {
	if((handle < 0) || (handle > MAX_OPEN_FILES))
        return;

    g_open_files[handle].valid = false;
    free(g_open_files[handle].file_name);
    free(g_open_files[handle].file_full_path);
}

bool isDirOpenByHandle(DIR2 handle) {
    if((handle < 0) || (handle > g_largest_dir_handle))
        return false;

    return g_open_dirs[handle].valid;
}

bool isDirOpenByFullPath(const char *full_path) {
	for (int i = 0; i <= g_largest_dir_handle; i++) {
		DIR_DESCRIPTOR *cur_dir = &g_open_dirs[i];

		if (!cur_dir->valid)
			continue;

		if (strcmp(full_path, cur_dir->dir_full_path) == 0)
			return true;
	}

	return false;
}

void removeFromOpenDirs(DIR2 handle) {
    if (handle < 0 || handle > g_largest_dir_handle)
        return;

    g_open_dirs[handle].valid = false;
    free(g_open_dirs[handle].dir_name);
    free(g_open_dirs[handle].dir_full_path);

    if (handle == g_largest_dir_handle) {
	DIR2 last_occupied_handle = findLastOccupiedDirHandle();
	if (last_occupied_handle < 0)
		last_occupied_handle = 0;

        g_largest_dir_handle = last_occupied_handle;
	}
}

void dumpBootBlockInfo() {
	printf("\n");
	printf("Dumping boot block info:\n");
	printf("Id: %.4s\n", g_boot_block.id);
	printf("Version: 0x%hx\n", g_boot_block.version);
	printf("Sectors per block: %hu\n", g_boot_block.blockSize);
	printf("MFT blocks: %hu\n", g_boot_block.MFTBlocksSize);
	printf("Disk blocks: %u\n", g_boot_block.diskSectorSize);
	printf("\n");
}

void dumpDirEntryInfo(DIRENT2 *dentry) {
	printf("\n");
	printf("Dumping dentry info:\n");
	printf("Name: %.51s\n", dentry->name);
	printf("Type: 0x%hhx\n", dentry->fileType);
	printf("Size: %hu\n", dentry->fileSize);
	printf("\n");
}

void dumpDirRecordInfo(RECORD *record) {
	printf("\n");
	printf("Dumping dir record info:\n");
	printf("Type: 0x%hhx\n", record->TypeVal);
	printf("Name: %.51s\n", record->name);
	printf("Size in blocks: %hu\n", record->blocksFileSize);
	printf("Size in bytes: %hu\n", record->bytesFileSize);
	printf("MFT number: %hu\n", record->MFTNumber);
	printf("\n");
}

void dumpFileDescriptorInfo(FILE_DESCRIPTOR *file) {
	printf("\n");
	printf("Dumping file descriptor info:\n");
	printf("Name: %.51s\n", file->file_name);
	printf("Path: %s\n", file->file_full_path);
	printf("Size in bytes: %hu\n", file->file_size);
	printf("Current position: %hu\n", file->current_position);
	printf("MFT number: %hu\n", file->mft_number);
	printf("Record disk offset: %u\n", file->record_offset);
	printf("\n");
}

void dumpDirDescriptorInfo(DIR_DESCRIPTOR *dir) {
	printf("\n");
	printf("Dumping dir descriptor info:\n");
	printf("Name: %.51s\n", dir->dir_name);
	printf("Path: %s\n", dir->dir_full_path);
	printf("Size in records: %hu\n", dir->dir_size);
	printf("Current position: %hu\n", dir->current_position);
	printf("MFT number: %hu\n", dir->mft_number);
	printf("Record disk offset: %u\n", dir->record_offset);
	printf("\n");
}

void dumpOpenFiles() {
	printf("\n");
	printf("Dumping open files...\n");

	for (int i = 0; i < MAX_OPEN_FILES; i++) {
		FILE_DESCRIPTOR *cur_file = &g_open_files[i];
		if (cur_file->valid)
			dumpFileDescriptorInfo(cur_file);
	}

	printf("Dumped open files.\n");
	printf("\n");
}

void dumpOpenDirs() {
	printf("\n");
	printf("Dumping open dirs...\n");

	for (int i = 0; i <= g_largest_dir_handle; i++) {
		DIR_DESCRIPTOR *cur_dir = &g_open_dirs[i];
		if (cur_dir->valid)
			dumpDirDescriptorInfo(cur_dir);
	}

	printf("Dumped open dirs.\n");
	printf("\n");
}

void dumpMftRegisterInfo(MFT_REGISTER *mft_register) {
	printf("\n");
	printf("Dumping register 4tuplas...\n");

	MFT_TUPLA *tupla;
	int i = 0;
	do {
		tupla = &mft_register->mft_tuplas[i];

		if (tupla->atributeType != -1)
			dumpMftTuplaInfo(tupla);

		i += 1;
	} while (i < 32 && (tupla->atributeType == 1 || tupla->atributeType == 2));

	printf("Dumped register 4tuplas.\n");
	printf("\n");
}

void dumpMftTuplaInfo(MFT_TUPLA *mft_tupla) {
	printf("\n");
	printf("Dumping tupla info:\n");
	printf("Type: 0x%hx\n", mft_tupla->atributeType);
	printf("VBN: %hu\n", mft_tupla->virtualBlockNumber);
	printf("LBN: %hu\n", mft_tupla->logicalBlockNumber);
	printf("Contiguous blocks: %hu\n", mft_tupla->numberOfContiguosBlocks);
	printf("\n");
}

void dumpBlockData(unsigned char *block_data) {
	printf("\n");
	printf("Dumping block data:\n");

	for (int i = 0; i < BLOCK_SIZE / 16; i++) {
		for (int j = 0; j < 16; j++) {
			printf("%.2hhx ", block_data[i*16 + j]);
		}
		printf("\n");
	}
	printf("\n");
}

int getMftTuplaIndexUsingVBN(DWORD VBN, MFT_REGISTER mft_reg) {
	for (int index = 0; index < 32; index++) {
		MFT_TUPLA tupla = mft_reg.mft_tuplas[index];

		// if it's a redir, recurse
		if (tupla.atributeType == 2) {
			int redir_mft_number = tupla.virtualBlockNumber;
			mft_reg = mftRegisterFromMftNumber(redir_mft_number);
			return getMftTuplaIndexUsingVBN(VBN, mft_reg);
		}

		// if it's invalid, we didn't find the dir, so stop
		if (tupla.atributeType == -1)
			return -1;

		int cur_VBN = tupla.virtualBlockNumber;
		int cur_contig = tupla.numberOfContiguosBlocks;
		if (VBN >= cur_VBN && VBN < cur_VBN + cur_contig)
			return index;
	}

	// not found
	return -2;
}

unsigned int findRegisterWithVBN(DWORD VBN, unsigned int start_mft_number) {
	MFT_REGISTER mft_reg = mftRegisterFromMftNumber(start_mft_number);

	for(int i = 0; i < 32; i++) {
		MFT_TUPLA *tupla = &mft_reg.mft_tuplas[i];

		// if it's a redir, recurse
		if (tupla->atributeType == 2) {
			int redir_mft_number = tupla->virtualBlockNumber;
			return findRegisterWithVBN(VBN, redir_mft_number);
		}

		// if it's invalid, we didn't find the dir, so stop
		if (tupla->atributeType == -1)
			return -1;

		int cur_VBN = tupla->virtualBlockNumber;
		int cur_contig = tupla->numberOfContiguosBlocks;
		if (VBN >= cur_VBN && VBN < cur_VBN + cur_contig)
			return start_mft_number;
	}

	// not found
	return -2;
}

unsigned int findTuplaInRegisterWithVBN(DWORD VBN, MFT_REGISTER mft_reg) {
	for (int i = 0; i < 32; i++) {
		MFT_TUPLA *tupla = &mft_reg.mft_tuplas[i];

		// if it's invalid, we didn't find the dir, so stop
		if (tupla->atributeType == -1)
			return -1;

		int this_VBN = tupla->virtualBlockNumber;
		int this_contig = tupla->numberOfContiguosBlocks;
		if (VBN >= this_VBN && VBN < this_VBN + this_contig)
			return i;
	}

	// not found
	return -1;
}

unsigned int findLBNInRegisterWithVBN(DWORD VBN, MFT_REGISTER mft_reg) {
	unsigned int tupla_idx = findTuplaInRegisterWithVBN(VBN, mft_reg);
	MFT_TUPLA *tupla = &mft_reg.mft_tuplas[tupla_idx];

	if (tupla->atributeType == -1)
		return -1;

	int contig_block = VBN - tupla->virtualBlockNumber;
	return tupla->logicalBlockNumber + contig_block;
}

bool is4tuplaEnded(MFT_TUPLA _4tupla, int number_of_swept_sectors) {
    DWORD block_size = g_boot_block.blockSize;
    int number_of_sectors_in_tuple = _4tupla.numberOfContiguosBlocks * block_size;
    
    return (number_of_swept_sectors >= number_of_sectors_in_tuple);
}

MFT_REGISTER mftRegisterFromSector(unsigned int sector_number) {
	MFT_REGISTER mft_register;
	unsigned char tmp_sector[SECTOR_SIZE];

	read_sector(sector_number, (void *)&tmp_sector);
	memcpy(&mft_register.mft_tuplas, tmp_sector, SECTOR_SIZE);

	read_sector(sector_number + 1, (void *)&tmp_sector);
	memcpy(&mft_register.mft_tuplas[16], tmp_sector, SECTOR_SIZE);

	return mft_register;
}

MFT_REGISTER mftRegisterFromMftNumber(unsigned int mft_number) {
	int mft_start_sector = g_boot_block.blockSize;
	int mft_register_sector = mft_start_sector + mft_number * (sizeof(MFT_REGISTER) / SECTOR_SIZE);

	return mftRegisterFromSector(mft_register_sector);
}

void readBlockByLBN(unsigned char *block, unsigned int LBN) {
	unsigned char tmp_sector[SECTOR_SIZE];

	int base_sector = LBN * g_boot_block.blockSize;
	for (int i = 0; i < g_boot_block.blockSize; i++) {
		read_sector(base_sector + i, (void *)&tmp_sector);
		memcpy(block + i*SECTOR_SIZE, tmp_sector, SECTOR_SIZE);
	}
}

void writeBlockToLBN(unsigned char *block, unsigned int LBN) {
	unsigned char tmp_sector[SECTOR_SIZE];

	int base_sector = LBN * g_boot_block.blockSize;
	for (int i = 0; i < g_boot_block.blockSize; i++) {
		memcpy(tmp_sector, block + i*SECTOR_SIZE, SECTOR_SIZE);
		write_sector(base_sector + i, (void *)&tmp_sector);
	}
}

unsigned int getFirstFreeMftNumber() {
    unsigned int mft_size_in_blocks = g_boot_block.MFTBlocksSize;
    unsigned int mft_size_in_bytes = mft_size_in_blocks * BLOCK_SIZE;
    unsigned int mft_register_size_in_bytes = 512;
    unsigned int total_registers_in_mft = mft_size_in_bytes / mft_register_size_in_bytes;

    for (unsigned int index = 4; index < total_registers_in_mft; index++) {
        MFT_REGISTER mft_reg = mftRegisterFromMftNumber(index);

        if (mft_reg.mft_tuplas[0].atributeType == -1)
             return index;
    }
    
    return -1;
}

RECORD readRecordFromBlockByNumber(unsigned char *block_buf, unsigned int record_num) {
	RECORD record;

	int record_offset_in_block = record_num * sizeof(RECORD);
	memcpy(&record, &block_buf[record_offset_in_block], sizeof(RECORD));

	return record;
}

RECORD readRecordFromDiskOffset(unsigned int disk_offset) {
	RECORD record;

    unsigned int sector_number = disk_offset / SECTOR_SIZE;
    unsigned int sector_offset = disk_offset % SECTOR_SIZE;
	unsigned char tmp_sector[SECTOR_SIZE];

	read_sector(sector_number, (void *)&tmp_sector);
	memcpy(&record, tmp_sector + sector_offset, sizeof(RECORD));

	return record;
}

int writeRecordToDiskOffset(RECORD *record, unsigned int disk_offset) {
    unsigned int boot_block_size = BLOCK_SIZE;
    unsigned int mft_size = g_boot_block.MFTBlocksSize * BLOCK_SIZE;
    if (disk_offset <= (boot_block_size + mft_size))
        return -1;

    // position must be multiple of 64; we don't wanna deal with sector splitting here
    if (disk_offset % sizeof(RECORD) != 0)
        return -2;

    unsigned int sector_number = disk_offset / SECTOR_SIZE;
    unsigned int sector_offset = disk_offset % SECTOR_SIZE;
	unsigned char tmp_sector[SECTOR_SIZE];

	read_sector(sector_number, (void *)&tmp_sector);
	memcpy(tmp_sector + sector_offset, record, sizeof(RECORD));
	write_sector(sector_number, (void *)&tmp_sector);

	return 0;
}

int invalidateRecordByDiskOffset(unsigned int disk_offset) {
    RECORD record = { .TypeVal = TYPEVAL_INVALIDO };
    return writeRecordToDiskOffset(&record, disk_offset);
}

unsigned int diskOffsetFromMftRegisterByNameAndType(MFT_REGISTER mft_register, char *name, int type) {
	RECORD dir_record;
	for (int i = 0; i < 32; i++) {
		// if it's a redir, follow it
		if (mft_register.mft_tuplas[i].atributeType == 2) {
			int redir_mft_number = mft_register.mft_tuplas[i].virtualBlockNumber;
			mft_register = mftRegisterFromMftNumber(redir_mft_number);
			return diskOffsetFromMftRegisterByNameAndType(mft_register, name, type);
		}

		// if it's invalid, we didn't find the dir, so stop
		if (mft_register.mft_tuplas[i].atributeType == -1)
			return -1;

		// if it's valid, load the contiguous blocks one by one
		int LBN = mft_register.mft_tuplas[i].logicalBlockNumber;
		int contig_blocks = mft_register.mft_tuplas[i].numberOfContiguosBlocks;
		for (int contig_block_num = 0; contig_block_num < contig_blocks; contig_block_num++) {
			unsigned char *block_buf = calloc(BLOCK_SIZE, sizeof(char));

			// read the block
			readBlockByLBN(block_buf, LBN + contig_block_num);

			// try to find the wanted dir in this block
			int cur_dentry_number = 0;
			while (cur_dentry_number * sizeof(RECORD) < BLOCK_SIZE) {
				dir_record = readRecordFromBlockByNumber(block_buf, cur_dentry_number);

				// found
				if (type == dir_record.TypeVal && strcmp(name, dir_record.name) == 0) {
					unsigned int LBN_offset = LBN * BLOCK_SIZE;
					return LBN_offset + cur_dentry_number * sizeof(RECORD);
				}

				cur_dentry_number += 1;
			}

			free(block_buf);
		}
	}

	// not found
	return -1;
}

unsigned int dirDiskOffsetFromMftRegisterByName(MFT_REGISTER mft_register, char *name) {
    return diskOffsetFromMftRegisterByNameAndType(mft_register, name, TYPEVAL_DIRETORIO);
}

unsigned int fileDiskOffsetFromMftRegisterByName(MFT_REGISTER mft_register, char *name) {
    return diskOffsetFromMftRegisterByNameAndType(mft_register, name, TYPEVAL_REGULAR);
}

void markFreeBlocksInBitmap(int LBN, int contig_blocks) {
    for (int block_number = LBN; block_number < LBN + contig_blocks; block_number++)
        setBitmap2(block_number, false);
}

bool isNextContiguousBlockFree(MFT_TUPLA mft_tupla) {
    int block_number = mft_tupla.logicalBlockNumber + mft_tupla.numberOfContiguosBlocks + 1;
    bool allocated = getBitmap2(block_number);

    return !allocated;
}

FILE2 findFirstFreeFileHandle() {
	for (int i = 0; i < MAX_OPEN_FILES; i++) {
		if (!g_open_files[i].valid)
			return i;
	}

	return -1;
}

DIR2 findFirstFreeDirHandle() {
	for (int i = 0; i <= g_largest_dir_handle; i++) {
		if (!g_open_dirs[i].valid)
			return i;
	}

	return -1;
}

DIR2 findLastOccupiedDirHandle() {
	int i;

	for (i = g_largest_dir_handle; i >= 0; i--) {
		if (g_open_dirs[i].valid)
			break;
	}

	return i;
}

FILE2 openFileUsingDirRecord(RECORD dir_record, char *file_full_path, DWORD record_offset) {
	FILE2 handle = findFirstFreeFileHandle();

	if (handle < 0)
		return -1;

	FILE_DESCRIPTOR *file = &g_open_files[handle];
	file->valid = true;
	int file_name_len = strlen(dir_record.name);
	file->file_name = malloc(file_name_len * sizeof(char));
    strcpy(file->file_name, dir_record.name);
    int file_full_path_len = strlen(file_full_path) + 1;
	file->file_full_path = malloc(file_full_path_len * sizeof(char));
    strcpy(file->file_full_path, file_full_path);
    file->file_size = dir_record.bytesFileSize;
    file->current_position = 0;
    file->mft_number = dir_record.MFTNumber;
    file->record_offset = record_offset;

	return handle;
}

DIR2 openDirUsingDirRecord(RECORD dir_record, char *dir_full_path, DWORD record_offset) {
	DIR2 handle = findFirstFreeDirHandle();

	if (handle < 0) {
		handle = g_largest_dir_handle + 1;
        g_open_dirs = realloc(g_open_dirs, handle * sizeof(DIR_DESCRIPTOR));
    }

	DIR_DESCRIPTOR *dir = &g_open_dirs[handle];
	dir->valid = true;
	int dir_name_len = strlen(dir_record.name) + 1;
	dir->dir_name = malloc(dir_name_len * sizeof(char));
    strcpy(dir->dir_name, dir_record.name);
    int full_path_len = strlen(dir_full_path);
	dir->dir_full_path = malloc(full_path_len * sizeof(char));
    strcpy(dir->dir_full_path, dir_full_path);
    dir->dir_size = dir_record.blocksFileSize * BLOCK_SIZE / sizeof(RECORD);
    dir->current_position = 0;
    dir->mft_number = dir_record.MFTNumber;
    dir->record_offset = record_offset;

	return handle;
}

DIR2 openRootDir() {
	DIR2 root_handle = findFirstFreeDirHandle();
	if (root_handle < 0) {
		root_handle = g_largest_dir_handle + 1;
        g_open_dirs = realloc(g_open_dirs, root_handle * sizeof(DIR_DESCRIPTOR));
    }

	DIR_DESCRIPTOR *root_dir = &g_open_dirs[root_handle];
	root_dir->valid = true;
	root_dir->dir_name = malloc(2 * sizeof(char));
    strcpy(root_dir->dir_name, "/");
	root_dir->dir_full_path = malloc(2 * sizeof(char));
    strcpy(root_dir->dir_full_path, "/");
    root_dir->dir_size = 16;
    root_dir->current_position = 0;
    root_dir->mft_number = 1;
    root_dir->record_offset = -1;

	return root_handle;
}

RECORD getRootDirRecord() {
	RECORD root_dir;
	strcpy(root_dir.name, "/");
	root_dir.blocksFileSize = 1;
	root_dir.bytesFileSize = 1024;
	root_dir.TypeVal = TYPEVAL_DIRETORIO;
    root_dir.MFTNumber = 1;
	return root_dir;
}

RECORD getLastDirRecordFromPath(char *pathname) {
	// copy pathname to something we can destroy
	int len = strlen(pathname) + 1;
	char *pathname_cpy = malloc(len * sizeof(char));
	strcpy(pathname_cpy, pathname);

	// base dir is "/"
	int mft_register_num = 1; // root MFT register

	// get a subdir name
	char *cur_dir_name = strtok(pathname_cpy, "/");

	// recurse until we find the last subdir in the path
	MFT_REGISTER mft_register;
	RECORD cur_dir = getRootDirRecord();
	while (cur_dir_name != NULL) {
		mft_register = mftRegisterFromMftNumber(mft_register_num);
		unsigned int cur_dir_offset = dirDiskOffsetFromMftRegisterByName(mft_register, cur_dir_name);
		cur_dir = readRecordFromDiskOffset(cur_dir_offset);
		mft_register_num = cur_dir.MFTNumber;
		cur_dir_name = strtok(NULL, "/");
	}

	free(pathname_cpy);
	return cur_dir;
}

void fillDirEntryFromRecord(RECORD *record, DIRENT2 *dentry) {
    strcpy(dentry->name, record->name);
    dentry->fileSize = record->bytesFileSize;
    dentry->fileType = record->TypeVal;
}

void writeRegister(MFT_REGISTER mft_register, unsigned int mft_number) {
	unsigned char tmp_sector[SECTOR_SIZE];

	int mft_start_sector = g_boot_block.blockSize;
	int mft_register_sector = mft_start_sector + mft_number * (sizeof(MFT_REGISTER) / SECTOR_SIZE);

	memcpy(tmp_sector, &mft_register.mft_tuplas, SECTOR_SIZE);
	write_sector(mft_register_sector, (void *)&tmp_sector);

	memcpy(tmp_sector, &mft_register.mft_tuplas[16], SECTOR_SIZE);
	write_sector(mft_register_sector + 1, (void *)&tmp_sector);
}

void createMftTupla(int mft_number) {
	MFT_TUPLA tupla;
	tupla.atributeType = 0;
	tupla.virtualBlockNumber = 0;
	tupla.logicalBlockNumber = 0;
	tupla.numberOfContiguosBlocks = 0;
	
	MFT_REGISTER reg;
	reg.mft_tuplas[0] = tupla;
	writeRegister(reg, mft_number);
}

void allocateMftTupla(int mft_number) {
	MFT_TUPLA tupla;
	tupla.atributeType = 1;
	tupla.virtualBlockNumber = 0;
	tupla.logicalBlockNumber = searchBitmap2(0);
	tupla.numberOfContiguosBlocks = 1;
	
	setBitmap2(tupla.logicalBlockNumber, 1);

	MFT_REGISTER reg;
	reg.mft_tuplas[0] = tupla;
	writeRegister(reg, mft_number);
}

void freeAllBlocksAndTuplasInRegisterByVBN(unsigned int start_mft_number,
										   unsigned int first_unneeded_VBN,
										   bool invalidate_before_recurse) {
	MFT_REGISTER mft_reg = mftRegisterFromMftNumber(start_mft_number);

	for (int i = 0; i < 32; i++) {
		MFT_TUPLA *tupla = &mft_reg.mft_tuplas[i];

		// if it's a redir, recurse
		if (tupla->atributeType == 2) {
			int redir_mft_number = tupla->virtualBlockNumber;

			if (invalidate_before_recurse)
				tupla->atributeType = -1;

			freeAllBlocksAndTuplasInRegisterByVBN(redir_mft_number,
												  first_unneeded_VBN,
												  invalidate_before_recurse);
		}

		// if it's invalid, stop
		if (tupla->atributeType == -1)
			break;

		unsigned int tupla_start_VBN = tupla->virtualBlockNumber;
		unsigned int tupla_start_LBN = tupla->logicalBlockNumber;
		unsigned int tupla_contig = tupla->numberOfContiguosBlocks;

		// if it's unneeded, free the blocks and invalidate
		if (tupla_start_VBN >= first_unneeded_VBN) {
			markFreeBlocksInBitmap(tupla_start_LBN, tupla_contig);
			tupla->atributeType = -1;
			continue;
		}

		// if it doesn't have to be changed, skip it
		if (tupla_start_VBN + tupla_contig < first_unneeded_VBN)
			continue;

		// otherwise, we must free just a few of its blocks
		unsigned int tupla_new_contig = first_unneeded_VBN - tupla_start_VBN;
		unsigned int first_unneeded_LBN = tupla_start_LBN + tupla_new_contig;
		markFreeBlocksInBitmap(first_unneeded_LBN, tupla_contig);
		tupla->numberOfContiguosBlocks = tupla_new_contig;
		invalidate_before_recurse = true;
	}

	// write changes
	writeRegister(mft_reg, start_mft_number);
}

bool isDirEmpty(RECORD dir) {
	unsigned char block_buf[BLOCK_SIZE];
	bool empty = true;

	/* for each block */
	for (int block_num = 0; block_num < dir.blocksFileSize; block_num++) {
		MFT_REGISTER mft_reg = mftRegisterFromMftNumber(dir.MFTNumber);
		unsigned int LBN = findLBNInRegisterWithVBN(block_num, mft_reg);
		readBlockByLBN(block_buf, LBN);

		/* for each record */
		int max_records = BLOCK_SIZE / sizeof(RECORD);
		for (int record_num = 0; record_num < max_records; record_num++) {
			RECORD record = readRecordFromBlockByNumber(block_buf, record_num);
			if (record.TypeVal == TYPEVAL_REGULAR
				|| record.TypeVal == TYPEVAL_DIRETORIO)
			{
				empty = false;
				break;
			}
		}
	}

	return empty;
}

unsigned int findFirstFreeDiskOffsetForRecordInDirRecord(RECORD dir) {
	MFT_REGISTER reg = mftRegisterFromMftNumber(dir.MFTNumber);
	DWORD dirBlocks = dir.blocksFileSize;

	for (int vbn = 0; vbn < dirBlocks; vbn++) {
		unsigned int lbn =  findLBNInRegisterWithVBN(vbn, reg);

		unsigned char *block = calloc(BLOCK_SIZE, sizeof(char));
		readBlockByLBN(block, lbn);

		int recordsPerBlocks = BLOCK_SIZE / sizeof(RECORD);
		for (int curRecordNumber = 0; curRecordNumber < recordsPerBlocks; curRecordNumber++) {
			RECORD record;
			memcpy(&record, block + curRecordNumber * sizeof(RECORD), sizeof(RECORD));
			if (record.TypeVal != 1 && record.TypeVal != 2)
				return lbn * BLOCK_SIZE + curRecordNumber * sizeof(RECORD);       
		}
	}

	return -1;
}


DWORD getLBNUsingVBN(DWORD mft_number, DWORD VBN) {
	MFT_REGISTER mft_reg = mftRegisterFromMftNumber(mft_number);

	int tupla_index = getMftTuplaIndexUsingVBN(VBN, mftRegisterFromMftNumber(mft_number));
	unsigned int VBN_distance_in_tupla = VBN - mft_reg.mft_tuplas[tupla_index].virtualBlockNumber;

	DWORD LBN = mft_reg.mft_tuplas[tupla_index].logicalBlockNumber + VBN_distance_in_tupla;

	return LBN;
}

DWORD getLBNUsingVBNAndMftTupla(DWORD mft_number, int tupla_index, DWORD VBN) {
	MFT_REGISTER mft_reg = mftRegisterFromMftNumber(mft_number);

	unsigned int VBN_distance_in_tupla = VBN - mft_reg.mft_tuplas[tupla_index].virtualBlockNumber;

	DWORD LBN = mft_reg.mft_tuplas[tupla_index].logicalBlockNumber + VBN_distance_in_tupla;

	return LBN;	
}

int getEndingTuplaIndexInMftRegister(MFT_REGISTER mft_reg) {
	for(int i = 0; i < 32; i++) {
		if ((mft_reg.mft_tuplas[i].atributeType == 0) || (mft_reg.mft_tuplas[i].atributeType == 0))
			return i;
	}
	return -1;
}

int allocateContiguousBlock(MFT_TUPLA *mft) {
    int block_number = mft->logicalBlockNumber + mft->numberOfContiguosBlocks;
    if(getBitmap2(block_number) == 0) {
	    setBitmap2(block_number, 1);
	    mft->numberOfContiguosBlocks += 1;
	    return 0;
	}
	else {
		return -1;
	}
}

int allocateBlocksInMft(MFT_REGISTER mft_reg, int mft_number, int number_of_blocks, DWORD VBN) {
	if (number_of_blocks == 0)
		return 0;

	printf("VBN: %d\n", VBN);

	int ending_tupla_index = getEndingTuplaIndexInMftRegister(mft_reg);

	if (mft_reg.mft_tuplas[ending_tupla_index].atributeType == 2)
		return allocateBlocksInMft(mftRegisterFromMftNumber(mft_reg.mft_tuplas[ending_tupla_index].virtualBlockNumber), mft_reg.mft_tuplas[ending_tupla_index].virtualBlockNumber, number_of_blocks, VBN);

	int new_mft_number;

	if (ending_tupla_index == 31) {
		//NO SPACE IN THIS REGISTER
		new_mft_number = getFirstFreeMftNumber();

		if (new_mft_number == -1)
			return -1;

		mft_reg.mft_tuplas[ending_tupla_index].atributeType = 2;
		mft_reg.mft_tuplas[ending_tupla_index].virtualBlockNumber = new_mft_number;
		mft_reg.mft_tuplas[ending_tupla_index].logicalBlockNumber = -1;
		mft_reg.mft_tuplas[ending_tupla_index].numberOfContiguosBlocks = -1;

		writeRegister(mft_reg, mft_number);
		return allocateBlocksInMft(mftRegisterFromMftNumber(mft_reg.mft_tuplas[ending_tupla_index].virtualBlockNumber), mft_reg.mft_tuplas[ending_tupla_index].virtualBlockNumber, number_of_blocks, VBN);
	}

	bool need_new_tupla = false;

	// new ending tupla
	mft_reg.mft_tuplas[ending_tupla_index + 1].atributeType = 0;

	int i;

	mft_reg.mft_tuplas[ending_tupla_index].atributeType = 1;
	mft_reg.mft_tuplas[ending_tupla_index].virtualBlockNumber = VBN;
	mft_reg.mft_tuplas[ending_tupla_index].logicalBlockNumber = searchBitmap2(0);
	mft_reg.mft_tuplas[ending_tupla_index].numberOfContiguosBlocks = 0;

	for(i=0; i < number_of_blocks; i++)
		if(allocateContiguousBlock(&mft_reg.mft_tuplas[ending_tupla_index]) == -1) {
			need_new_tupla = true;
			break;
		}

	writeRegister(mft_reg, mft_number);

	if (need_new_tupla)
		return allocateBlocksInMft(mft_reg, mft_number, number_of_blocks - i, VBN + i);

	return 0;	
}

void writeDataUsingMFT(MFT_REGISTER mft_reg, int starting_tupla, DWORD starting_VBN, DWORD starting_byte, char *buffer, int size, bool new_blocks) {
	int starting_LBN = mft_reg.mft_tuplas[starting_tupla].logicalBlockNumber + starting_VBN - mft_reg.mft_tuplas[starting_tupla].virtualBlockNumber;

	unsigned char* first_block = calloc(BLOCK_SIZE, sizeof(char));
	readBlockByLBN(first_block, starting_LBN);
	if (size < BLOCK_SIZE)
		memcpy(first_block + starting_byte, buffer, size);
	else 
		memcpy(first_block + starting_byte, buffer, BLOCK_SIZE - starting_byte);
	writeBlockToLBN(first_block, starting_LBN);

	int no=0;
	int written_blocks = 1;
	int current_tupla = starting_tupla+1;
	unsigned char* part_of_buffer_to_write = calloc(BLOCK_SIZE, sizeof(char));

	while (written_blocks * BLOCK_SIZE < size) {

		while ((current_tupla < 31) && (mft_reg.mft_tuplas[current_tupla].atributeType == 1)) {
			int current_block = 0;

			while ((current_block < mft_reg.mft_tuplas[current_tupla].numberOfContiguosBlocks) && (written_blocks * BLOCK_SIZE < size)) {
				
				int LBN = mft_reg.mft_tuplas[current_tupla].logicalBlockNumber + current_block;

				if ((written_blocks+1) * BLOCK_SIZE < size) {
					memcpy(part_of_buffer_to_write, buffer + written_blocks * BLOCK_SIZE, BLOCK_SIZE);
				}
				else {
					if (new_blocks == false) {
						readBlockByLBN(part_of_buffer_to_write, LBN);
						memcpy(part_of_buffer_to_write, buffer + written_blocks * BLOCK_SIZE, size - written_blocks * BLOCK_SIZE);
					}
					else {
						unsigned char* other_buffer = calloc(BLOCK_SIZE, sizeof(char));
						memcpy(other_buffer, buffer + written_blocks * BLOCK_SIZE, size - written_blocks * BLOCK_SIZE);

						no = 1;
						writeBlockToLBN(other_buffer, LBN);
						free(other_buffer);
					}
				}

				if(no ==0)
					writeBlockToLBN(part_of_buffer_to_write, LBN);

				written_blocks++;
				current_block++;
			}
			
			current_tupla++;
		}

		if (mft_reg.mft_tuplas[current_tupla].atributeType == 2) {
			mft_reg = mftRegisterFromMftNumber(mft_reg.mft_tuplas[current_tupla].virtualBlockNumber);
			current_tupla = 0;
		}
	}

	free(first_block);
	free(part_of_buffer_to_write);
}
