#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "t2fs_helper.h"

int main() {
	int ret = rmdir2("/etc_dir");
	printf("rmdir returned: %d\n", ret);

	FILE2 f = open2("/etc_dir/LongFile");
	dumpOpenFiles();

	ret = delete2("/etc_dir/LongFile");
	printf("delete returned: %d\n", ret);

	ret = close2(f);
	printf("close returned: %d\n", ret);

	MFT_REGISTER mft_reg = mftRegisterFromMftNumber(7);
	dumpMftRegisterInfo(&mft_reg);

	mft_reg = mftRegisterFromMftNumber(8);
	dumpMftRegisterInfo(&mft_reg);

	ret = delete2("/etc_dir/LongFile");
	printf("delete returned: %d\n", ret);

	mft_reg = mftRegisterFromMftNumber(7);
	dumpMftRegisterInfo(&mft_reg);

	mft_reg = mftRegisterFromMftNumber(8);
	dumpMftRegisterInfo(&mft_reg);

	ret = rmdir2("/etc_dir");
	printf("rmdir returned: %d\n", ret);

    return 0;
}
