#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "t2fs_helper.h"
#include "apidisk.h"

char WOMANIZER[2859] = "Superstar Where you from, how's it going? I know you Got a clue, what you doing?  You can play brand new to All the other chicks out here But I know what you are What you are, baby  Look at you Gettin' more than just a re-up Baby you Got all the puppets with their strings up  Fakin' like a good one But I call 'em like I see 'em I know what you are What you are, baby  Womanizer, woman-womanizer You're a womanizer Oh, womanizer, oh You're a womanizer, baby  You, you, you are You, you, you are Womanizer, womanizer Womanizer  Boy don't try to front, uh, I Know just, just, what you are, ah, ah Boy don't try to front, uh, I Know just, just, what you are, ah, ah You got me going You're oh so charming But I can't do it You womanizer Boy don't try to front, uh, I Know just, just, what you are, ah, ah Boy don't try to front, uh, I Know just, just, what you are, ah, ah  You say I'm crazy I got you crazy You're nothing but a Womanizer  Daddy-O You got the swagger of a champion Too bad for you You just can't find the right companion  I guess when you have one too many Makes it hard, it could be easy Who you are That's just who you are, baby  Lollipop Must mistake me, you're the sucker To think that I Would be a victim, not another  Say it, play it, how you wanna? But no way I'm ever gonna Fall for you Never you, baby  Womanizer, woman-womanizer You're a womanizer Oh, womanizer, oh You're a womanizer, baby  You, you, you are You, you, you are Womanizer, womanizer Womanizer  Boy don't try to front, uh, I Know just, just, what you are, ah, ah Boy don't try to front, uh, I Know just, just, what you are, ah, ah You got me going You're oh so charming But I can't do it You womanizer Boy don't try to front, uh, I Know just, just, what you are, ah, ah Boy don't try to front, uh, I Know just, just, what you are, ah, ah  You say I'm crazy I got you crazy You're nothing but a Womanizer  Maybe if We both lived in a Different world (Womanizer, womanizer, womanizer, womanizer)  It would be all good And maybe I could be your girl But I can't 'Cause we don't  Womanizer, woman-womanizer You're a womanizer Oh, womanizer, oh You're a womanizer, baby  You, you, you are You, you, you are Womanizer, womanizer Womanizer  Boy don't try to front, uh, I Know just, just, what you are, ah, ah Boy don't try to front, uh, I Know just, just, what you are, ah, ah You got me going You're oh so charming But I can't do it You womanizer Boy don't try to front, uh, I Know just, just, what you are, ah, ah Boy don't try to front, uh, I Know just, just, what you are, ah, ah  You say I'm crazy I got you crazy You're nothing but a Womanizer  Boy don't try to front, uh, I Know just, just, what you are, ah, ah Boy don't try to front, uh, I Know just, just, what you are, ah, ah  Womanizer, woman-womanizer You're a womanizer Oh, womanizer, oh You're a womanizer, baby";



int main() {
	int ret;
	DIR2 dh;
	FILE2 fh;

	printf("Beginning tests...\n");

	printf("dh = opendir2(\"X\") -> ");
	dh = opendir2("X");
	printf("returned %d\n", dh);

	printf("ret = closedir2(dh) -> ");
	ret = closedir2(dh);
	printf("returned %d\n", ret);

	printf("dh = opendir2(\"/\") -> ");
	dh = opendir2("/");
	printf("returned %d\n", dh);

	DIRENT2 dentry;
	do {
		printf("ret = readdir2(dh, &dentry) -> ");
		ret = readdir2(dh, &dentry);
		printf("returned %d\n", ret);

		if (dentry.fileType != 1)
			continue;

		char filepath[MAX_FILE_NAME_SIZE];
		strcpy(filepath, "/");
		strcat(filepath, dentry.name);

		printf("fh = open2(\"%s\") -> ", filepath);
		fh = open2(filepath);
		printf("returned %d\n", fh);

		char buf[3000];
		memset(buf, 0, sizeof(buf));

		printf("ret = seek2(fh, 0) -> ");
		ret = seek2(fh, 0);
		printf("returned %d\n", ret);

		printf("ret = read2(fh, buf, 3000) -> ");
		ret = read2(fh, buf, 3000);
		printf("returned %d\n", ret);

		printf("First 3000 bytes in file as string: \"%s\"\n", buf);

		memset(buf, 0, sizeof(buf));

		printf("ret = seek2(fh, 0) -> ");
		ret = seek2(fh, 0);
		printf("returned %d\n", ret);

		printf("Writing string into file... return -> ");
        ret = write2(fh, "MURILO", 6);
        printf("%d\n", ret);

		printf("ret = seek2(fh, 0) -> ");
		ret = seek2(fh, 0);
		printf("returned %d\n", ret);

		printf("ret = read2(fh, buf, 3000) -> ");
		ret = read2(fh, buf, 3000);
		printf("returned %d\n", ret);

		printf("First 3000 bytes in file as string: \"%s\"\n", buf);

		memset(buf, 0, sizeof(buf));

		printf("ret = seek2(fh, 6) -> ");
		ret = seek2(fh, 6);
		printf("returned %d\n", ret);

		printf("Writing string into file... return -> ");
        ret = write2(fh, "JESSICA", 7);
        printf("%d\n", ret);

		printf("ret = seek2(fh, 0) -> ");
		ret = seek2(fh, 0);
		printf("returned %d\n", ret);

		printf("ret = read2(fh, buf, 3000) -> ");
		ret = read2(fh, buf, 3000);
		printf("returned %d\n", ret);

		printf("First 3000 bytes in file as string: \"%s\"\n", buf);

		memset(buf, 0, sizeof(buf));

		printf("ret = seek2(fh, -1) -> ");
		ret = seek2(fh, -1);
		printf("returned %d\n", ret);

		printf("Writing string into file... return -> ");
        ret = write2(fh, "DANIEL", 6);
        printf("%d\n", ret);

		printf("ret = seek2(fh, 0) -> ");
		ret = seek2(fh, 0);
		printf("returned %d\n", ret);

		printf("ret = read2(fh, buf, 3000) -> ");
		ret = read2(fh, buf, 3000);
		printf("returned %d\n", ret);

		printf("First 3000 bytes in file as string: \"%s\"\n", buf);

		memset(buf, 0, sizeof(buf));

		printf("ret = seek2(fh, 0) -> ");
		ret = seek2(fh, 0);
		printf("returned %d\n", ret);

		printf("Writing string into file... return -> ");
        ret = write2(fh, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABB", 51);
        printf("%d\n", ret);

		printf("ret = seek2(fh, 0) -> ");
		ret = seek2(fh, 0);
		printf("returned %d\n", ret);

		printf("ret = read2(fh, buf, 3000) -> ");
		ret = read2(fh, buf, 3000);
		printf("returned %d\n", ret);

		printf("First 3000 bytes in file as string: \"%s\"\n", buf);

		memset(buf, 0, sizeof(buf));

		printf("ret = seek2(fh, 0) -> ");
		ret = seek2(fh, 0);
		printf("returned %d\n", ret);

		printf("Writing britney spears song into file... return -> ");
        ret = write2(fh, WOMANIZER, strlen(WOMANIZER));
        printf("%d\n", ret);

		printf("ret = seek2(fh, 0) -> ");
		ret = seek2(fh, 0);
		printf("returned %d\n", ret);

		printf("ret = read2(fh, buf, 3000) -> ");
		ret = read2(fh, buf, 3000);
		printf("returned %d\n", ret);

		printf("First 3000 bytes in file as string: \"%s\"\n", buf);

		memset(buf, 0, sizeof(buf));

		printf("ret = seek2(fh, 1025) -> ");
		ret = seek2(fh, 1025);
		printf("returned %d\n", ret);

		printf("Writing string into file... return -> ");
        ret = write2(fh, "ABCDEFGHIJ", 10);
        printf("%d\n", ret);

		printf("ret = seek2(fh, 0) -> ");
		ret = seek2(fh, 0);
		printf("returned %d\n", ret);

		printf("ret = read2(fh, buf, 3000) -> ");
		ret = read2(fh, buf, 3000);
		printf("returned %d\n", ret);

		printf("First 3000 bytes in file as string: \"%s\"\n", buf);

		memset(buf, 0, sizeof(buf));

		printf("ret = close2(fh) -> ");
		ret = close2(fh);
		printf("returned %d\n", ret);
	} while (dentry.fileType != TYPEVAL_INVALIDO);

	printf("ret = closedir2(dh) -> ");
	ret = closedir2(dh);
	printf("returned %d\n", ret);

	printf("Tests done!\n");

    return 0;
}
